#include <iostream>
#include <cstdlib>
#include <fstream>
#include <math.h>
using namespace std;

int sample[2][4] = { 44000, 48000, 32000,0,
22050,24000,16000,0 };

int bitrate[5][16]{ 0,32,64,96,128,160,192,224,256,288,320,352,384,416,448,0,
0,32,48,56,64,80,96,112,128,160,192,224,256,320,384,0,
0,32,40,48,56,64,80,96,112,128,160,192,224,256,320,0,
0,32,48,56,64,80,96,112,128,144,160,176,192,224,256,0,
0,8,16,24,32,40,48,56,64,80,96,112,128,144,160,0 };

int *chartohex(unsigned char arg[], int a, int tab[], int rozmiar)
{
	for (int i = 0; i<rozmiar; i++)
	{
		tab[i] = (int)arg[a + i];
		if (tab[i]<0)
			tab[i] = tab[i] + 256;
	}
};

int chunk(int tab[], int a, int i)
{
	int wynik = 0;
	for (; i != 0; i--)
		wynik = wynik + tab[a + i] * pow(256, i - 1);
	return wynik;
};

void GetBits(unsigned char zdekodowane[], char arg[], int a)
{
	char zmienik;
	zmienik = arg[a + 1];
	zdekodowane[0] = zmienik << 3; zdekodowane[0] = zdekodowane[0] >> 6;//B https://www.mp3-tech.org/programmer/frame_header.html Explanation to the symbols
	zmienik = arg[a + 1];
	zdekodowane[1] = zmienik << 5; zdekodowane[1] = zdekodowane[1] >> 6;//C
	zmienik = arg[a + 1];
	zdekodowane[2] = zmienik << 7; zdekodowane[2] = zdekodowane[2] >> 7;//D
	zmienik = arg[a + 2];
	zdekodowane[3] = zmienik >> 4; zdekodowane[3] = zdekodowane[3] << 4; zdekodowane[3] = zdekodowane[3] >> 4;//E
	zmienik = arg[a + 2];
	zdekodowane[4] = zmienik << 4; zdekodowane[4] = zdekodowane[4] >> 6;//F
	zmienik = arg[a + 2];
	zdekodowane[5] = zmienik << 6; zdekodowane[5] = zdekodowane[5] >> 7;//G 
	zmienik = arg[a + 2];
	zdekodowane[6] = zmienik << 7; zdekodowane[6] = zdekodowane[6] >> 7;//H
	zmienik = arg[a + 3];
	zdekodowane[7] = zmienik >> 6; zdekodowane[7] = zdekodowane[7] << 6; zdekodowane[7] = zdekodowane[7] >> 6;//I
	zmienik = arg[a + 3];
	zdekodowane[8] = zmienik << 2; zdekodowane[8] = zdekodowane[8] >> 6;//J
	zmienik = arg[a + 3];
	zdekodowane[9] = zmienik << 4; zdekodowane[9] = zdekodowane[9] >> 7;//K
	zmienik = arg[a + 3];
	zdekodowane[10] = zmienik << 5; zdekodowane[10] = zdekodowane[10] >> 7;//L
	zmienik = arg[a + 3];
	zdekodowane[11] = zmienik << 6; zdekodowane[11] = zdekodowane[11] >> 6;//M
};

void dekodowaniemp3(char arg[], int a, unsigned char zdekodowane[], int rozmiar) {
	int tab[12];
	GetBits(zdekodowane, arg, a);
	chartohex(zdekodowane, 0, tab, 12);
	int layer = 0, mpeg = 0, mpegsam = 0;
	switch (tab[0])
	{
	case 0:
		cout << "MPEG 2.5" << endl;
		mpeg = 3;
		break;
	case 1:
		cout << "Reserved" << endl;
		break;
	case 2:
		cout << "MPEG 2" << endl;
		mpeg = 2;
		mpegsam = 1;
		break;
	case 3:
		cout << "MPEG 1" << endl;
		break;
	}
	switch (tab[1])
	{
	case 0:
		cout << "Reserved" << endl;
		break;
	case 1:
		cout << "Layer 3" << endl;
		layer = layer + 2;
		break;
	case 2:
		cout << "Layer 2" << endl;
		layer = layer + 1;
		break;
	case 3:
		cout << "Layer 1" << endl;
		break;
	}
	switch (tab[2])
	{
	case 0:
		cout << "Protected by CRC" << endl;
		break;
	case 1:
		cout << "Not protected" << endl;
		break;
	}
	layer = layer + mpeg;
	cout << "Bitrate " << dec << bitrate[layer][tab[3]] << endl;
	cout << "Sampling rate " << dec << sample[mpegsam][tab[4]] << "Hz" << endl;
	if (tab[5] == 0)
		cout << "frame is padded" << endl;
	else
		cout << "Frame is not padded" << endl;
	cout << "Private bit " << tab[6] << endl;
	switch (tab[7])
	{
	case 0:
		cout << "Stereo" << endl;
		break;
	case 1:
		cout << "Joint Stereo" << endl;
		break;
	case 2:
		cout << "2 Mono" << endl;
		break;
	case 3:
		cout << "Mono" << endl;
		break;
	}
	if (tab[7] == 1)
	{
		cout << "Joint stereo information ";
		switch (tab[8])
		{
		case 0:
			cout << "Intensity Stereo-OFF MS Stereo-OFF" << endl;
			break;
		case 1:
			cout << "Intensity Stereo-ON MS Stereo-OFF" << endl;
			break;
		case 2:
			cout << "Intensity Stereo-OFF MS Stereo-ON" << endl;
			break;
		case 3:
			cout << "Intensity Stereo-ON MS Stereo-ON" << endl;
			break;
		}
	}
	if (tab[9] == 0)
		cout << "Audio is not copyrighted" << endl;
	else
		cout << "Audio is copyrighted" << endl;
	if (tab[10] == 0)
		cout << "Copy of original media" << endl;
	else
		cout << "Original media" << endl;
	cout << "Emphasis ";
	switch (tab[11])
	{
	case 0:
		cout << "NONE" << endl;
		break;
	case 1:
		cout << "50/50 ms" << endl;
		break;
	case 2:
		cout << "Reserved" << endl;
		break;
	case 3:
		cout << "CCIT J.17" << endl;
		break;
	}
	cout << "Rozmiar pliku " << rozmiar << " bajtow" << endl;
	rozmiar = (rozmiar * 8) / (bitrate[layer][tab[3]] * 1024);
	cout << "Dlugosc utworu " << rozmiar << "s" << endl;
}

void dekodowaniejpg(char arg[], int a) {
	int tab[20], data = 0;
	unsigned char kod[20];
	for (int i = 0; i<20; i++)
		kod[i] = arg[i];
	chartohex(kod, a, tab, 20);
	data = tab[18] * tab[19] + 16;
	if (data != 16)
	{
		unsigned char *kod = new unsigned char[data];
		for (int i = 0; i<4 + data; i++)
			kod[i] = arg[i];
		chartohex(kod, a, tab, 4 + data);
	}
	cout << "Start of image " << hex << tab[0] << " " << tab[1] << endl;
	cout << "APPO " << hex << tab[2] << " " << tab[3] << endl;
	cout << "APP0 length " << dec << tab[4] << " " << tab[5] << endl;
	cout << "Identyfikator " << hex << tab[6] << " " << tab[7] << " " << tab[8] << " " << tab[9] << " " << tab[10] << endl;
	cout << "Wersja " << tab[11] << ".0" << tab[12] << endl;
	cout << "Jednoska gestosci ";
	switch (tab[13])
	{
	case 0:
		cout << "Brak" << endl;
		break;
	case 1:
		cout << "Cale" << endl;
		break;
	case 2:
		cout << "Cm" << endl;
		break;
	}
	cout << "Rozdzielczosc w poziomie " << tab[14] + tab[15] << endl;
	cout << "Rozdzielczosc w pionie " << tab[16] + tab[17] << endl;
	cout << "Xthumbnail " << tab[18] << endl;
	cout << "Ythumbnail " << tab[19] << endl;
}

void dekodowaniewav(char arg[], int a) {
	unsigned char kod[46];
	int zdekodowane[46];
	for (int i = 0; i<46; i++)
		kod[i] = arg[i];
	chartohex(kod, a, zdekodowane, 46);
	cout << "Pierwsze cztery bajty " << arg[0] << arg[1] << arg[2] << arg[3] << endl;
	cout << "Chunk size " << dec << chunk(zdekodowane, 3, 4) << endl;
	cout << "Format: " << arg[8] << arg[9] << arg[10] << arg[11] << endl;
	cout << "SubchunkID: " << arg[12] << arg[13] << arg[14] << arg[15] << endl;
	cout << "Subchunk1Size " << dec << chunk(zdekodowane, 15, 4) << endl;
	if (zdekodowane[20] != 1)
		cout << "Compresed file!!!" << endl;
	else
		cout << "PCM" << endl;
	cout << "NumChannels: " << dec << chunk(zdekodowane, 21, 2) << endl;
	cout << "SampleRate: " << dec << chunk(zdekodowane, 23, 4) << endl;
	cout << "ByteRate: " << dec << chunk(zdekodowane, 27, 4) << endl;
	cout << "BlockAlign: " << dec << chunk(zdekodowane, 31, 2) << endl;
	cout << "BitsPerSample: " << dec << chunk(zdekodowane, 33, 2) << endl;
	cout << "Subchunk2ID " << arg[36] << arg[37] << arg[38] << arg[39] << endl;
	cout << "Subchunk2Size " << dec << chunk(zdekodowane, 39, 4) << endl;
}

int main(int argc, char** argv) {
	fstream plik;
	plik.open("3.mp3", ios::in | ios::binary);
	if (plik.good() == true)
		cout << "Uzyskano dostep do pliku!" << std::endl;
	else
		cout << "Dostep do pliku zostal zabroniony!" << std::endl;
	plik.seekg(0, ios::end);
	int rozmiar = plik.tellg();
	char *bufor = new char[rozmiar];
	plik.seekg(0, ios::beg);
	plik.read(bufor, rozmiar);
	int tab[4];
	unsigned char zdekodowane[12];
	for (int i = 0; i<rozmiar; i++)
	{
		int decoded = (int)bufor[i];
		if (decoded == -1)
		{
			//dekodowaniejpg(bufor,i);
			dekodowaniemp3(bufor, i, zdekodowane, rozmiar);
			break;
		}
	}
	return 0;
}